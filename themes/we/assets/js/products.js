(function($) {
	
	/** 
	 * Select2
	 */
	function formatSelectOptions (option) {
	  if (!option.id) { return option.text; }
	  var image = $(option.element).data('image');
	  if(!image){
	    return option.text;
	  }
	  var $html = $(
	    '<img src="' + image + '" class="option-image" /><span class="option-text">' + option.text + '</span>'
	  );
	  return $html;
	};

	$("select").select2({
	  templateResult: formatSelectOptions,
	  minimumResultsForSearch: 20
	});

	/** 
	 * Select disabling/enable
	 */
	$(".has-dependendee").on("change", function () {
	  $('[data-dependency=' + $(this).attr('id') + ']').prop("disabled", false);
	});

	/** 
	 * Product ajax filters
	 */
	$(".product-filters select").on("change", function () {
		$('#products').addClass('loading');
		setTimeout(function(){
			$('#products').load( "/products-ajax", function() {
				$('#products').removeClass('loading');			
			});
		}, 1000);
	});

	$(document).ready(function(){
		$('#products').find('.product').velocity("transition.fadeIn", { stagger: 250 });
	});

})(jQuery);