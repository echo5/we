(function($) {
  $('.collapse').on('show.bs.collapse', function () {
    $(this).siblings('.card-header').addClass('active');
  });

  $('.collapse').on('hide.bs.collapse', function () {
    $(this).siblings('.card-header').removeClass('active');
  });

  $('.accordion').find('[data-toggle="collapse"]').hover(function(e){
    e.stopPropagation();
  	if (!$(this).parent().parent().hasClass('active')) {
	  	$(this).trigger('click');
  	}
  });

})(jQuery);